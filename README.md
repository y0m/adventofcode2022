# Advent of Code 2022
## Informations
Most puzzles work with **stable** Rust, but:
* day 3
* t.b.d. ...

which have to be run with **nightly**, example:
```shell
cargo +nightly run --bin day03
```
