use std::collections::HashSet;
use std::error::Error;
use std::fmt;
use std::fs::read_to_string;
use std::path::Path;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    println!("{}", parta("./data/04/input.txt")?.len());
    println!("{}", partb("./data/04/input.txt")?.len());
    Ok(())
}

#[derive(Debug)]
struct AssignmentError;
impl Error for AssignmentError {}
impl fmt::Display for AssignmentError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "unknown assignment format")
    }
}

struct Assignment(HashSet<usize>);
impl FromStr for Assignment {
    type Err = AssignmentError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let range = s
            .split('-')
            .flat_map(str::parse::<usize>)
            .collect::<Vec<_>>();
        if range.len() != 2 {
            return Err(AssignmentError);
        }
        Ok(Self((range[0]..=range[1]).collect::<HashSet<_>>()))
    }
}

#[derive(Debug)]
struct PairError;
impl Error for PairError {}
impl fmt::Display for PairError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "unknown pair format")
    }
}

struct Pair {
    assignments: Vec<Assignment>,
}
impl FromStr for Pair {
    type Err = PairError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let assignments = s
            .split(',')
            .flat_map(str::parse::<Assignment>)
            .collect::<Vec<_>>();
        if assignments.len() != 2 {
            return Err(PairError);
        }
        Ok(Self { assignments })
    }
}

impl Pair {
    fn fully_contains(&self) -> bool {
        self.assignments.iter().map(|a| a.0.len()).any(|l| {
            l == self.assignments[0]
                .0
                .intersection(&self.assignments[1].0)
                .count()
        })
    }

    fn overlaps(&self) -> bool {
        self.assignments[0]
            .0
            .intersection(&self.assignments[1].0)
            .count()
            != 0
    }
}

fn parta<P: AsRef<Path>>(filename: P) -> Result<Vec<Pair>, Box<dyn Error>> {
    Ok(read_to_string(filename)?
        .lines()
        .flat_map(str::parse::<Pair>)
        .filter(|p| p.fully_contains())
        .collect())
}

fn partb<P: AsRef<Path>>(filename: P) -> Result<Vec<Pair>, Box<dyn Error>> {
    Ok(read_to_string(filename)?
        .lines()
        .flat_map(str::parse::<Pair>)
        .filter(|p| p.overlaps())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta() {
        let v = parta("./data/04/sample.txt").unwrap();
        assert_eq!(v.len(), 2);
    }

    #[test]
    fn test_partb() {
        let v = partb("./data/04/sample.txt").unwrap();
        assert_eq!(v.len(), 4);
    }
}
