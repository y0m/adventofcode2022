use std::error::Error;
use std::fs::read_to_string;
use std::path::Path;

fn main() -> Result<(), Box<dyn Error>> {
    println!("{:?}", parta("./data/01/input.txt")?);
    println!("{:?}", partb("./data/01/input.txt")?);
    Ok(())
}

fn parta<P: AsRef<Path>>(filename: P) -> Result<usize, Box<dyn Error>> {
    Ok(read_to_string(filename)?
        .split("\n\n")
        .map(|vs| vs.lines().flat_map(str::parse::<usize>).sum::<usize>())
        .max()
        .unwrap())
}

fn partb<P: AsRef<Path>>(filename: P) -> Result<usize, Box<dyn Error>> {
    let mut cals = read_to_string(filename)?
        .split("\n\n")
        .map(|vs| vs.lines().flat_map(str::parse::<usize>).sum::<usize>())
        .collect::<Vec<_>>();
    cals.sort_unstable_by(|a, b| b.cmp(a));
    Ok(cals.iter().take(3).sum::<usize>())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_parta() {
        let cal = parta("./data/01/sample.txt").unwrap();
        assert_eq!(cal, 24000);
    }

    #[test]
    fn sample_partb() {
        let cal = partb("./data/01/sample.txt").unwrap();
        assert_eq!(cal, 45000);
    }
}
