#![feature(iter_array_chunks)]
use std::{error::Error, fs::read_to_string, path::Path};

fn main() -> Result<(), Box<dyn Error>> {
    println!("{}", parta("./data/03/input.txt")?);
    println!("{}", partb("./data/03/input.txt")?);
    Ok(())
}

fn score(c: char) -> i32 {
    match c {
        'a'..='z' => (c as i32 - 'a' as i32) + 1,
        'A'..='Z' => (c as i32 - 'A' as i32) + 27,
        _ => 0,
    }
}

fn parta<P: AsRef<Path>>(filename: P) -> Result<i32, Box<dyn Error>> {
    Ok(read_to_string(filename)?
        .lines()
        .flat_map(|l| {
            let (lh, rh) = l.split_at(l.len() / 2);
            lh.chars().find(|&c| rh.contains(c))
        })
        .map(score)
        .sum())
}

fn partb<P: AsRef<Path>>(filename: P) -> Result<i32, Box<dyn Error>> {
    Ok(read_to_string(filename)?
        .lines()
        .array_chunks()
        .flat_map(|[a, b, c]| a.chars().find(|&ch| b.contains(ch) && c.contains(ch)))
        .map(score)
        .sum())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta() {
        let tot = parta("./data/03/sample.txt").unwrap();
        assert_eq!(tot, 157);
    }

    #[test]
    fn test_partb() {
        let tot = partb("./data/03/sample.txt").unwrap();
        assert_eq!(tot, 70);
    }
}
