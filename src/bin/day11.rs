use std::error::Error;
use std::fs::read_to_string;
use std::path::Path;
use std::rc::Rc;
use std::str::FromStr;
use std::usize;

fn main() -> Result<(), Box<dyn Error>> {
    println!("{}", parta("./data/11/input.txt")?);
    println!("{}", partb("./data/11/input.txt")?);
    Ok(())
}

fn parta<P>(filename: P) -> Result<usize, Box<dyn Error>>
where
    P: AsRef<Path>,
{
    let src = read_to_string(filename)?;
    let monkeys = src
        .split("\n\n")
        .flat_map(str::parse::<Monkey>)
        .collect::<Vec<_>>();

    rounds(monkeys, 20, |val| val / 3)
}

fn partb<P>(filename: P) -> Result<usize, Box<dyn Error>>
where
    P: AsRef<Path>,
{
    let src = read_to_string(filename)?;
    let monkeys = src
        .split("\n\n")
        .flat_map(str::parse::<Monkey>)
        .collect::<Vec<_>>();

    let factor = monkeys.iter().map(|m| m.divisible_by).product::<usize>();
    rounds(monkeys, 10000, |val| val % factor)
}

fn rounds<F>(mut monkeys: Vec<Monkey>, count: usize, regul: F) -> Result<usize, Box<dyn Error>>
where
    F: Fn(usize) -> usize,
{
    for _r in 1..=count {
        let len = monkeys.len();
        for m in 0..len {
            let mut curm = monkeys[m].clone();
            curm.inspected += curm.items.len();
            for item in curm.items.drain(..) {
                let new = match curm.op.rhs {
                    Some(rhs) => (curm.op.opfn)(item, rhs),
                    None => (curm.op.opfn)(item, item),
                };
                let new = regul(new);
                let nextm = match new % curm.divisible_by == 0 {
                    true => curm.true_throw,
                    false => curm.false_throw,
                };
                monkeys[nextm].items.push(new);
            }
            monkeys[m].items = curm.items;
            monkeys[m].inspected = curm.inspected;
        }
    }
    monkeys.sort_unstable_by(|a, b| b.inspected.cmp(&a.inspected));
    let Some(first) = monkeys.first() else {
        return Err("first monkey not found".into());
    };
    let Some(second) = monkeys.get(1) else {
        return Err("second monkey not found".into());
    };
    Ok(first.inspected * second.inspected)
}

#[derive(Clone)]
struct Operation {
    rhs: Option<usize>,
    opfn: Rc<Box<dyn Fn(usize, usize) -> usize>>,
}

#[derive(Clone)]
struct Monkey {
    items: Vec<usize>,
    op: Operation,
    divisible_by: usize,
    true_throw: usize,
    false_throw: usize,
    inspected: usize,
}

impl FromStr for Monkey {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut items: Vec<usize> = vec![];
        let mut operation: Option<Operation> = None;
        let mut divisible_by: usize = 0;
        let mut true_throw: usize = 0;
        let mut false_throw: usize = 0;
        let mut lines = s.lines();
        lines.next();
        for line in lines {
            let Some((left, right)) = line.trim().split_once(": ") else {
                return Err("line format error".into());
            };
            match left {
                "Starting items" => {
                    items.extend_from_slice(
                        &(right
                            .split(", ")
                            .flat_map(str::parse::<usize>)
                            .collect::<Vec<_>>()),
                    );
                }
                "Operation" => {
                    let Some((_lh, rh)) = right.split_once(" = ") else {
                        return Err("operation format error".into());
                    };
                    let ops = rh.split_whitespace().collect::<Vec<_>>();
                    let Some(&op) = ops.get(1) else {
                        return Err("operand format error".into());
                    };
                    let opfn = match op {
                        "*" => usize::wrapping_mul,
                        "+" => usize::wrapping_add,
                        _ => return Err("unknown operand".into()),
                    };
                    let Some(&op) = ops.get(2)  else{
                        return Err("rhs operand format error".into());
                    };
                    let rhs = match op {
                        "old" => None,
                        s => s.parse::<usize>().ok(),
                    };
                    operation = Some(Operation {
                        rhs,
                        opfn: Rc::new(Box::new(opfn)),
                    });
                }
                "Test" => {
                    let Some(divby) = right.split_whitespace().last().map(|v| v.parse::<usize>()) else {
                        return Err("test line format error".into());
                    };
                    divisible_by = divby.map_err(|err| err.to_string())?;
                }
                "If true" => {
                    let Some(next) = right.split_whitespace().last().map(|v| v.parse::<usize>()) else {
                        return Err("true test line format error".into());
                    };
                    true_throw = next.map_err(|err| err.to_string())?;
                }
                "If false" => {
                    let Some(next) = right.split_whitespace().last().map(|v| v.parse::<usize>()) else {
                        return Err("false test line format error".into());
                    };
                    false_throw = next.map_err(|err| err.to_string())?;
                }
                _ => return Err("Monkey formart error".into()),
            }
        }
        let Some(op) = operation else {
            return Err("operation not found".into());
        };
        Ok(Monkey {
            items,
            op,
            divisible_by,
            true_throw,
            false_throw,
            inspected: 0,
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta() {
        assert_eq!(parta("./data/11/sample.txt").unwrap(), 10605);
    }

    #[test]
    fn test_partb() {
        assert_eq!(partb("./data/11/sample.txt").unwrap(), 2713310158);
    }
}
