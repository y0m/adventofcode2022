use std::collections::VecDeque;
use std::error::Error;
use std::fs::read_to_string;
use std::path::Path;

fn main() -> Result<(), Box<dyn Error>> {
    println!("{}", parta("./data/07/input.txt")?);
    println!("{}", partb("./data/07/input.txt")?);
    Ok(())
}

#[derive(Debug)]
struct Dir {
    #[allow(dead_code)]
    name: String,
    entries: Vec<Entry>,
}

impl Dir {
    fn size(&self) -> usize {
        self.entries
            .iter()
            .map(|e| match e {
                Entry::File(f) => f.size,
                Entry::Dir(d) => d.size(),
            })
            .sum()
    }
}

#[derive(Debug)]
struct File {
    #[allow(dead_code)]
    name: String,
    size: usize,
}

#[derive(Debug)]
enum Entry {
    Dir(Dir),
    File(File),
}

fn create_dir<'a>(
    dirname: &str,
    mut lines: VecDeque<&'a str>,
) -> Result<(Dir, VecDeque<&'a str>), Box<dyn Error>> {
    let mut entries = vec![];
    while let Some(line) = lines.pop_front() {
        let mut sp = line.split(' ');
        match sp.next() {
            Some("$") => {
                if let Some("cd") = sp.next() {
                    match sp.next() {
                        Some("..") => break,
                        Some(name) => {
                            let (dir, leftlines) = create_dir(name, lines)?;
                            lines = leftlines;
                            entries.push(Entry::Dir(dir));
                        }
                        _ => return Err("dir name not found".into()),
                    }
                }
            }
            Some("dir") => {}
            Some(s) => {
                let size = s.parse::<usize>()?;
                let Some(name) = sp.next() else {
                    return Err("filename missing".into());
                };
                entries.push(Entry::File(File {
                    name: name.to_owned(),
                    size,
                }));
            }
            _ => return Err("shell output format error".into()),
        }
    }
    Ok((
        Dir {
            name: dirname.to_owned(),
            entries,
        },
        lines,
    ))
}

fn load_fs<P: AsRef<Path>>(filename: P) -> Result<Dir, Box<dyn Error>> {
    let src = read_to_string(filename)?;
    let mut lines = src.lines().collect::<VecDeque<_>>();
    lines.pop_front();
    let (root, _) = create_dir("/", lines)?;
    Ok(root)
}

fn parta<P: AsRef<Path>>(filename: P) -> Result<usize, Box<dyn Error>> {
    let rootdir = load_fs(filename)?;
    Ok(sum_dir_inf_100000(&rootdir.entries))
}

fn sum_dir_inf_100000(entries: &[Entry]) -> usize {
    entries
        .iter()
        .flat_map(|e| {
            let Entry::Dir(d) = e else {
                return None;
            };
            if d.size() <= 100000 {
                return Some(d.size() + sum_dir_inf_100000(&d.entries));
            }
            Some(sum_dir_inf_100000(&d.entries))
        })
        .sum::<usize>()
}

fn partb<P: AsRef<Path>>(filename: P) -> Result<usize, Box<dyn Error>> {
    let rootdir = load_fs(filename)?;
    let leftspace = 70_000_000 - rootdir.size();
    let missing = 30_000_000 - leftspace;
    let Some(&min) = find_dir_sup_missing(&rootdir.entries, missing).iter().min() else {
        return Err("no values array".into());
    };
    Ok(min)
}

fn find_dir_sup_missing(entries: &[Entry], missing: usize) -> Vec<usize> {
    entries
        .iter()
        .flat_map(|e| {
            let Entry::Dir(d) = e else {
                return None;
            };
            if d.size() < missing {
                return None;
            }
            let mut sub = find_dir_sup_missing(&d.entries, missing);
            sub.push(d.size());
            Some(sub)
        })
        .flatten()
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta() {
        assert_eq!(parta("./data/07/sample.txt").unwrap(), 95437)
    }

    #[test]
    fn test_partb() {
        assert_eq!(partb("./data/07/sample.txt").unwrap(), 24933642)
    }
}
