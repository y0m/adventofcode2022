use std::error::Error;
use std::fmt;
use std::fs::read_to_string;
use std::path::Path;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    println!("{}", parta("./data/10/input.txt")?);
    partb("./data/10/sample.txt")?;
    partb("./data/10/input.txt")?;
    Ok(())
}

struct Cpu<'a> {
    cycle: usize,
    x: isize,
    watch: Vec<usize>,
    snapshot: Vec<(usize, isize)>,
    subscribers: Vec<Box<dyn FnMut(usize, isize) + 'a>>,
}

enum Op {
    Noop,
    Addx(isize),
}

impl FromStr for Op {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut sp = s.split(' ');
        match sp.next() {
            Some(opstr) => match opstr {
                "noop" => Ok(Op::Noop),
                "addx" => match sp.next() {
                    Some(valstr) => Ok(Op::Addx(
                        valstr
                            .parse::<isize>()
                            .map_err(|_| "value format error".to_string())?,
                    )),

                    _ => Err("value not found for addx op".into()),
                },
                _ => Err("unknown op".into()),
            },
            _ => Err("no op found".into()),
        }
    }
}

impl<'a> Cpu<'a> {
    fn new() -> Self {
        let watch = vec![20, 60, 100, 140, 180, 220];
        Self {
            cycle: 1,
            x: 1,
            watch,
            snapshot: vec![],
            subscribers: vec![],
        }
    }

    fn signal_strength(&self) -> isize {
        self.snapshot
            .iter()
            .map(|&(cycle, x)| (cycle as isize) * x)
            .sum()
    }

    fn run(&mut self, ops: &[Op]) {
        for op in ops {
            match op {
                Op::Noop => self.inc_cycle(1),
                Op::Addx(val) => {
                    self.inc_cycle(2);
                    self.x += val;
                }
            }
        }
    }

    fn inc_cycle(&mut self, count: usize) {
        for _i in 0..count {
            if self.watch.contains(&self.cycle) {
                self.snapshot.push((self.cycle, self.x));
            }
            for cb in &mut self.subscribers {
                (*cb)(self.cycle, self.x);
            }
            self.cycle += 1;
        }
    }

    fn register(&mut self, cb: impl FnMut(usize, isize) + 'a) {
        self.subscribers.push(Box::new(cb));
    }
}

fn parta<P: AsRef<Path>>(filename: P) -> Result<isize, Box<dyn Error>> {
    let src = read_to_string(filename)?;
    let ops = src.lines().flat_map(str::parse::<Op>).collect::<Vec<_>>();
    let mut cpu = Cpu::new();
    cpu.run(&ops);
    Ok(cpu.signal_strength())
}

struct Crt {
    buffer: String,
}

impl Crt {
    fn new() -> Self {
        Self {
            buffer: ".".repeat(40 * 6),
        }
    }

    fn cycle(&mut self, cycle: usize, x: isize) {
        let s: usize = if (x - 1) < 0 { 0 } else { (x - 1) as usize };
        let e: usize = if (x + 1) < 0 { 0 } else { (x + 1) as usize };
        let pos = (cycle - 1) % 40;
        if (s..=e).contains(&pos) {
            self.buffer.replace_range((cycle - 1)..cycle, "#");
        }
    }

    fn cycle_callback(&mut self) -> impl FnMut(usize, isize) + '_ {
        move |cycle, x| self.cycle(cycle, x)
    }
}

impl fmt::Display for Crt {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (idx, c) in self.buffer.char_indices() {
            write!(f, "{}", c)?;
            if (idx + 1) % 40 == 0 {
                writeln!(f)?;
            }
        }
        Ok(())
    }
}

fn partb<P: AsRef<Path>>(filename: P) -> Result<(), Box<dyn Error>> {
    let src = read_to_string(filename)?;
    let ops = src.lines().flat_map(str::parse::<Op>).collect::<Vec<_>>();

    let mut crt = Crt::new();
    let cb = crt.cycle_callback();

    {
        let mut cpu = Cpu::new();
        cpu.register(cb);
        cpu.run(&ops);
    }

    println!("{crt}");

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta() {
        assert_eq!(parta("./data/10/sample.txt").unwrap(), 13140);
    }
}
