use std::error::Error;
use std::fmt;
use std::fs::read_to_string;
use std::path::Path;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    println!("{}", parta("./data/05/input.txt")?);
    println!("{}", partb("./data/05/input.txt")?);
    Ok(())
}

#[derive(Debug, Clone)]
struct Stacks(Vec<String>);

impl FromStr for Stacks {
    type Err = StacksError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lines = s.lines().collect::<Vec<_>>();
        let stackscount = match lines.pop() {
            Some(v) => v.split(' ').filter(|sub| !sub.is_empty()).count(),
            None => return Err(StacksError),
        };
        let mut stacks = vec![String::new(); stackscount];
        for l in lines {
            let mut l = l.to_owned();
            for stack in stacks.iter_mut() {
                if l.len() < 3 {
                    return Err(StacksError);
                }
                let crat = l.drain(..3).collect::<String>();
                if !crat.trim().is_empty() {
                    let sqbr: &[_] = &['[', ']'];
                    stack.insert(
                        0,
                        crat.trim_matches(sqbr)
                            .chars()
                            .next()
                            .map_or(Err(StacksError), Ok)?,
                    );
                }
                if l.is_empty() {
                    break;
                }
                l.drain(..1);
            }
        }
        Ok(Stacks(stacks))
    }
}

#[derive(Debug)]
struct StacksError;
impl Error for StacksError {}
impl fmt::Display for StacksError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Stacks format error")
    }
}

#[derive(Debug, Default)]
struct Instruction {
    r#move: usize,
    from: usize,
    to: usize,
}

impl Instruction {
    fn new(r#move: usize, from: usize, to: usize) -> Self {
        Self { r#move, from, to }
    }
}

#[derive(Debug)]
struct InstructionError;
impl Error for InstructionError {}
impl fmt::Display for InstructionError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "instruction format error")
    }
}

impl FromStr for Instruction {
    type Err = InstructionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut r#move: Option<usize> = None;
        let mut from: Option<usize> = None;
        let mut to: Option<usize> = None;
        let mut sp = s.split(' ');
        while let Some(inst) = sp.next() {
            match inst {
                "move" => match sp.next() {
                    Some(v) => r#move = Some(v.parse::<usize>().map_err(|_| InstructionError)?),
                    None => return Err(InstructionError),
                },
                "from" => match sp.next() {
                    Some(v) => from = Some(v.parse::<usize>().map_err(|_| InstructionError)?),
                    None => return Err(InstructionError),
                },
                "to" => match sp.next() {
                    Some(v) => to = Some(v.parse::<usize>().map_err(|_| InstructionError)?),
                    None => return Err(InstructionError),
                },
                _ => return Err(InstructionError),
            }
        }
        if r#move.is_none() || from.is_none() || to.is_none() {
            return Err(InstructionError);
        }
        Ok(Instruction::new(
            r#move.unwrap(),
            from.unwrap(),
            to.unwrap(),
        ))
    }
}

fn crates_instructions<P: AsRef<Path>>(
    filename: P,
) -> Result<(Stacks, Vec<Instruction>), Box<dyn Error>> {
    let src = read_to_string(filename)?;
    let mut sp = src.split("\n\n");
    let stacks = match sp.next() {
        Some(s) => s.parse::<Stacks>()?,
        None => return Err("input format error".into()),
    };
    let instructions = match sp.next() {
        Some(s) => s
            .lines()
            .flat_map(str::parse::<Instruction>)
            .collect::<Vec<_>>(),
        None => return Err("input format error".into()),
    };
    Ok((stacks, instructions))
}

// CrateMover 9000
fn parta<P: AsRef<Path>>(filename: P) -> Result<String, Box<dyn Error>> {
    let (mut stacks, instructions) = crates_instructions(filename)?;
    for inst in instructions {
        for _m in 0..inst.r#move {
            let Some(c) = stacks.0[inst.from - 1].pop() else {
                continue;
            };
            stacks.0[inst.to - 1].push(c);
        }
    }
    Ok(stacks.0.iter().flat_map(|s| s.chars().last()).collect())
}

// CrateMover 9001
fn partb<P: AsRef<Path>>(filename: P) -> Result<String, Box<dyn Error>> {
    let (mut stacks, instructions) = crates_instructions(filename)?;
    for inst in instructions {
        let mut from = stacks.0[inst.from - 1].clone();
        let to = &mut stacks.0[inst.to - 1];
        to.push_str(&(from.drain((from.len() - inst.r#move)..).collect::<String>()));
        stacks.0[inst.from - 1] = from;
    }
    Ok(stacks.0.iter().flat_map(|s| s.chars().last()).collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta() {
        let res = parta("./data/05/sample.txt").unwrap();
        assert_eq!(&res, "CMZ");
    }

    #[test]
    fn test_partb() {
        let res = partb("./data/05/sample.txt").unwrap();
        assert_eq!(&res, "MCD");
    }
}
