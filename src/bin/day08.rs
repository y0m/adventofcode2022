use std::error::Error;
use std::fs::read_to_string;
use std::path::Path;
use std::time::Instant;

fn main() -> Result<(), Box<dyn Error>> {
    let a = Instant::now();
    println!("{}", parta("./data/08/input.txt")?);
    let elasped = Instant::elapsed(&a);
    eprintln!("{:?}", elasped);
    let a = Instant::now();
    println!("{}", partb("./data/08/input.txt")?);
    let elasped = Instant::elapsed(&a);
    eprintln!("{:?}", elasped);
    Ok(())
}

fn parta<P: AsRef<Path>>(filename: P) -> Result<usize, Box<dyn Error>> {
    let src = read_to_string(filename)?;
    let trees = src
        .lines()
        .map(|l| {
            l.as_bytes()
                .iter()
                .map(|&b| b as i32 - b'0' as i32)
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();
    let edge = edge_count(&trees);
    let visibles = visible_trees_from_edge(&trees);
    Ok(edge + visibles)
}

fn edge_count(trees: &[Vec<i32>]) -> usize {
    let rows = trees.len();
    let Some(cols) = trees.first().map(|r1| r1.len()) else {
        return 0;
    };
    ((rows - 1) + (cols - 1)) * 2
}

fn visible_trees_from_edge(trees: &[Vec<i32>]) -> usize {
    let rows = trees.len();
    let Some(cols) = trees.first().map(|r1| r1.len()) else {
        return 0;
    };
    let mut visibility = vec![vec![0usize; cols]; rows];

    trees.iter().enumerate().skip(1).for_each(|(rowidx, row)| {
        if rowidx >= (rows - 1) {
            return;
        }
        row.iter().enumerate().skip(1).for_each(|(colidx, tree)| {
            if colidx >= (cols - 1) {
                return;
            }
            //from the left
            match row[..colidx].iter().max() {
                Some(max) if max < tree => {
                    visibility[rowidx][colidx] += 1;
                    return;
                }
                _ => {}
            }
            //from the right
            match row[(colidx + 1)..].iter().max() {
                Some(max) if max < tree => {
                    visibility[rowidx][colidx] += 1;
                    return;
                }
                _ => {}
            }
            //from the top
            match trees[..rowidx].iter().map(|row| row[colidx]).max() {
                Some(max) if max < *tree => {
                    visibility[rowidx][colidx] += 1;
                    return;
                }
                _ => {}
            }
            //from the bottom
            match trees[(rowidx + 1)..].iter().map(|row| row[colidx]).max() {
                Some(max) if max < *tree => {
                    visibility[rowidx][colidx] += 1;
                }
                _ => {}
            }
        });
    });

    visibility
        .iter()
        .map(|row| row.iter().filter(|&&v| v > 0).count())
        .sum()
}

fn partb<P: AsRef<Path>>(filename: P) -> Result<usize, Box<dyn Error>> {
    let src = read_to_string(filename)?;
    let trees = src
        .lines()
        .map(|l| {
            l.as_bytes()
                .iter()
                .map(|&b| b as i32 - b'0' as i32)
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();
    Ok(max_scenic_score(&trees))
}

fn max_scenic_score(trees: &[Vec<i32>]) -> usize {
    let rows = trees.len();
    let Some(cols) = trees.first().map(|r1| r1.len()) else {
        return 0;
    };
    let mut scenic = vec![vec![0usize; cols]; rows];

    trees.iter().enumerate().skip(1).for_each(|(rowidx, row)| {
        if rowidx >= (rows - 1) {
            return;
        }
        row.iter().enumerate().skip(1).for_each(|(colidx, tree)| {
            if colidx >= (cols - 1) {
                return;
            }
            //to the left
            let l = row[..colidx]
                .iter()
                .rposition(|t| t >= tree)
                .map_or(colidx, |pos| colidx - pos);
            //to the right
            let r = row[(colidx + 1)..]
                .iter()
                .position(|t| t >= tree)
                .map_or(cols - colidx - 1, |pos| pos + 1);
            //to the top
            let t = trees[..rowidx]
                .iter()
                .map(|row| row[colidx])
                .rposition(|t| t >= *tree)
                .map_or(rowidx, |pos| rowidx - pos);
            //to the bottom
            let b = trees[(rowidx + 1)..]
                .iter()
                .map(|row| row[colidx])
                .position(|t| t >= *tree)
                .map_or(rows - rowidx - 1, |pos| pos + 1);

            scenic[rowidx][colidx] = l * r * t * b;
        });
    });

    scenic
        .iter()
        .map(|row| row.iter().max().map_or(0, |&v| v))
        .max()
        .map_or(0, |v| v)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta() {
        assert_eq!(parta("./data/08/sample.txt").unwrap(), 21);
    }

    #[test]
    fn test_partb() {
        assert_eq!(partb("./data/08/sample.txt").unwrap(), 8);
    }
}
