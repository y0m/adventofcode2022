use std::collections::HashSet;
use std::error::Error;
use std::fs::read_to_string;
use std::path::Path;

fn main() -> Result<(), Box<dyn Error>> {
    println!("{:?}", parta("./data/06/input.txt")?);
    println!("{:?}", partb("./data/06/input.txt")?);
    Ok(())
}

fn parta<P: AsRef<Path>>(filename: P) -> Result<Option<usize>, Box<dyn Error>> {
    let src = read_to_string(filename)?;
    Ok(bitwise_start_of::<4>(&src))
}

fn partb<P: AsRef<Path>>(filename: P) -> Result<Option<usize>, Box<dyn Error>> {
    let src = read_to_string(filename)?;
    Ok(bitwise_start_of::<14>(&src))
}

fn hs_start_of<const N: usize>(src: &str) -> Option<usize> {
    let mut i = 0;
    while i < src.len() - N {
        let set = &src[i..i + N].chars().collect::<HashSet<_>>();
        if set.len() == N {
            return Some(i + N);
        }
        i += 1;
    }
    None
}

fn bitwise_start_of<const N: usize>(src: &str) -> Option<usize> {
    src.as_bytes()
        .windows(N)
        .position(move |set| {
            let mut data: usize = 0;
            for &c in set {
                let prev = data;
                data |= 1 << (c - b'a');
                if prev == data {
                    return false;
                }
            }
            true
        })
        .map(|p| p + N)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta_1() {
        assert_eq!(
            bitwise_start_of::<4>("bvwbjplbgvbhsrlpgdmjqwftvncz"),
            Some(5)
        );
    }

    #[test]
    fn test_parta_2() {
        assert_eq!(
            bitwise_start_of::<4>("nppdvjthqldpwncqszvftbrmjlhg"),
            Some(6)
        );
    }

    #[test]
    fn test_parta_3() {
        assert_eq!(
            bitwise_start_of::<4>("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"),
            Some(10)
        );
    }

    #[test]
    fn test_parta_4() {
        assert_eq!(
            bitwise_start_of::<4>("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"),
            Some(11)
        );
    }

    #[test]
    fn test_partb_1() {
        assert_eq!(
            bitwise_start_of::<14>("mjqjpqmgbljsphdztnvjfqwrcgsmlb"),
            Some(19)
        );
    }

    #[test]
    fn test_partb_2() {
        assert_eq!(
            bitwise_start_of::<14>("bvwbjplbgvbhsrlpgdmjqwftvncz"),
            Some(23)
        );
    }

    #[test]
    fn test_partb_3() {
        assert_eq!(
            bitwise_start_of::<14>("nppdvjthqldpwncqszvftbrmjlhg"),
            Some(23)
        );
    }

    #[test]
    fn test_partb_4() {
        assert_eq!(
            bitwise_start_of::<14>("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"),
            Some(29)
        );
    }

    #[test]
    fn test_partb_5() {
        assert_eq!(
            bitwise_start_of::<14>("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"),
            Some(26)
        );
    }
}
