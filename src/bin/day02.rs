use std::fmt;
use std::fs::read_to_string;
use std::path::Path;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("{}", parta("./data/02/input.txt")?);
    println!("{}", partb("./data/02/input.txt")?);
    Ok(())
}

fn parta<P: AsRef<Path>>(filename: P) -> Result<i32, Box<dyn std::error::Error>> {
    Ok(read_to_string(filename)?
        .lines()
        .flat_map(str::parse::<Hands>)
        .map(myscore)
        .sum::<i32>())
}

fn partb<P: AsRef<Path>>(filename: P) -> Result<i32, Box<dyn std::error::Error>> {
    Ok(read_to_string(filename)?
        .lines()
        .flat_map(str::parse::<HandChoice>)
        .map(|v| myscore(v.into_hands()))
        .sum::<i32>())
}

fn myscore(hand: Hands) -> i32 {
    let score = match hand.0[1].cmp(&hand.0[0]) {
        std::cmp::Ordering::Greater => 6,
        std::cmp::Ordering::Equal => 3,
        std::cmp::Ordering::Less => 0,
    };
    score + (hand.0[1] as i32)
}

#[derive(Debug, Eq, Clone, Copy)]
enum Hand {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

#[derive(Debug)]
struct UnknownHandError;
impl fmt::Display for UnknownHandError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "unknown hand")
    }
}

impl std::error::Error for UnknownHandError {}

impl FromStr for Hand {
    type Err = UnknownHandError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "X" | "A" => Ok(Hand::Rock),
            "Y" | "B" => Ok(Hand::Paper),
            "Z" | "C" => Ok(Hand::Scissors),
            _ => Err(UnknownHandError),
        }
    }
}

impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        *self as i32 == *other as i32
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        if self == other {
            std::cmp::Ordering::Equal
        } else if (self == &Hand::Rock && other == &Hand::Scissors)
            || (self == &Hand::Scissors && other == &Hand::Paper)
            || (self == &Hand::Paper && other == &Hand::Rock)
        {
            std::cmp::Ordering::Greater
        } else {
            std::cmp::Ordering::Less
        }
    }
}

struct Hands(Vec<Hand>);

#[derive(Debug)]
struct HandsError;
impl fmt::Display for HandsError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "unknown hands")
    }
}
impl std::error::Error for HandsError {}

impl FromStr for Hands {
    type Err = HandsError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Hands(
            s.split_whitespace()
                .flat_map(str::parse::<Hand>)
                .collect::<Vec<_>>(),
        ))
    }
}

enum Choice {
    Win,
    Lose,
    Draw,
}

impl Choice {
    fn against(&self, other: &Hand) -> Hand {
        match self {
            Choice::Win => match other {
                Hand::Rock => Hand::Paper,
                Hand::Paper => Hand::Scissors,
                Hand::Scissors => Hand::Rock,
            },
            Choice::Lose => match other {
                Hand::Rock => Hand::Scissors,
                Hand::Paper => Hand::Rock,
                Hand::Scissors => Hand::Paper,
            },
            Choice::Draw => *other,
        }
    }
}

#[derive(Debug)]
struct ChoiceError;
impl fmt::Display for ChoiceError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "unknown choice")
    }
}
impl std::error::Error for ChoiceError {}

impl FromStr for Choice {
    type Err = ChoiceError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "X" => Ok(Choice::Lose),
            "Y" => Ok(Choice::Draw),
            "Z" => Ok(Choice::Win),
            _ => Err(ChoiceError),
        }
    }
}

struct HandChoice((Hand, Choice));

impl HandChoice {
    fn into_hands(self) -> Hands {
        let (hand, choice) = self.0;
        let mine = choice.against(&hand);
        Hands(vec![hand, mine])
    }
}

#[derive(Debug)]
struct HandChoiceError;
impl fmt::Display for HandChoiceError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "unknown hands")
    }
}
impl std::error::Error for HandChoiceError {}

impl FromStr for HandChoice {
    type Err = HandChoiceError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut s = s.split_whitespace();
        let hand = s.next().map_or(Err(HandChoiceError), |v| {
            v.parse::<Hand>().or(Err(HandChoiceError))
        })?;
        let choice = s.next().map_or(Err(HandChoiceError), |v| {
            v.parse::<Choice>().or(Err(HandChoiceError))
        })?;
        Ok(HandChoice((hand, choice)))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta() {
        let res = parta("./data/02/sample.txt").unwrap();
        assert_eq!(res, 15);
    }

    #[test]
    fn test_partb() {
        let res = partb("./data/02/sample.txt").unwrap();
        assert_eq!(res, 12);
    }
}
