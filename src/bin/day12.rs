use std::collections::HashSet;
use std::error::Error;
use std::fs::read_to_string;
use std::path::Path;

fn main() -> Result<(), Box<dyn Error>> {
    println!("{}", parta("./data/12/input.txt")?);
    println!("{}", partb("./data/12/input.txt")?);
    Ok(())
}

#[derive(Debug)]
struct Pos {
    row: usize,
    col: usize,
    distance: usize,
}

impl Pos {
    fn new(row: usize, col: usize, distance: usize) -> Self {
        Self { row, col, distance }
    }
}

fn parta<P: AsRef<Path>>(filename: P) -> Result<usize, Box<dyn Error>> {
    let src = read_to_string(filename)?;
    let map = src.lines().map(|l| l.as_bytes()).collect::<Vec<&[u8]>>();
    find_distance(&map, b'S', b'E', false)
}

fn partb<P: AsRef<Path>>(filename: P) -> Result<usize, Box<dyn Error>> {
    let src = read_to_string(filename)?;
    let map = src.lines().map(|l| l.as_bytes()).collect::<Vec<&[u8]>>();
    find_distance(&map, b'E', b'a', true)
}

fn find_distance(
    map: &[&[u8]],
    start: u8,
    end: u8,
    reverse: bool,
) -> Result<usize, Box<dyn Error>> {
    let Some(mypos) = map.iter().enumerate().find_map(|(rowidx, &row)| {
        row.iter().enumerate().find_map(|(colidx, &col)| {
            if col == start {
                Some((rowidx, colidx))
            } else {
                None
            }
        })
    })
    else {
        return Err("start not found".into());
    };
    let mut visited = HashSet::new();
    let mut q = vec![Pos {
        row: mypos.0,
        col: mypos.1,
        distance: 0,
    }];
    let distance = loop {
        match search_end(map, &mut visited, q, end, reverse) {
            Ok(d) => break d,
            Err(newq) => q = newq,
        }
    };
    Ok(distance)
}

fn search_end(
    map: &[&[u8]],
    visited: &mut HashSet<(usize, usize)>,
    mut q: Vec<Pos>,
    end: u8,
    reverse: bool,
) -> Result<usize, Vec<Pos>> {
    let len = q.len();
    for i in 0..len {
        if !visited.insert((q[i].row, q[i].col)) {
            continue;
        }
        if map[q[i].row][q[i].col] == end {
            return Ok(q[i].distance);
        }
        let neighbors = neighbors(map, visited, (q[i].row, q[i].col), reverse);
        let distance = q[i].distance;
        q.extend(
            neighbors
                .iter()
                .map(|pos| Pos::new(pos.0, pos.1, distance + 1)),
        );
    }
    Err(q)
}

fn neighbors(
    map: &[&[u8]],
    visited: &HashSet<(usize, usize)>,
    pos: (usize, usize),
    reverse: bool,
) -> Vec<(usize, usize)> {
    let mut neighbors = vec![];
    if is_walkable(map, visited, map[pos.0][pos.1], (pos.0, pos.1 + 1), reverse) {
        neighbors.push((pos.0, pos.1 + 1));
    }
    if pos.1 > 0 && is_walkable(map, visited, map[pos.0][pos.1], (pos.0, pos.1 - 1), reverse) {
        neighbors.push((pos.0, pos.1 - 1));
    }
    if is_walkable(map, visited, map[pos.0][pos.1], (pos.0 + 1, pos.1), reverse) {
        neighbors.push((pos.0 + 1, pos.1));
    }
    if pos.0 > 0 && is_walkable(map, visited, map[pos.0][pos.1], (pos.0 - 1, pos.1), reverse) {
        neighbors.push((pos.0 - 1, pos.1));
    }
    neighbors
}

fn is_walkable(
    map: &[&[u8]],
    visited: &HashSet<(usize, usize)>,
    cur: u8,
    pos: (usize, usize),
    reverse: bool,
) -> bool {
    if pos.0 >= map.len() {
        return false;
    }
    if pos.1 >= map[0].len() {
        return false;
    }
    if visited.get(&pos).is_some() {
        return false;
    }
    if !reverse && get_byte(map[pos.0][pos.1]) <= get_byte(cur) {
        return true;
    }
    if reverse && get_byte(map[pos.0][pos.1]) >= get_byte(cur) {
        return true;
    }
    if (get_byte(cur) as isize - get_byte(map[pos.0][pos.1]) as isize).abs() == 1 {
        return true;
    }
    false
}

fn get_byte(byte: u8) -> u8 {
    match byte {
        b'S' => b'a',
        b'E' => b'z',
        _ => byte,
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta() {
        assert_eq!(parta("./data/12/sample.txt").unwrap(), 31);
    }

    #[test]
    fn test_partb() {
        assert_eq!(partb("./data/12/sample.txt").unwrap(), 29);
    }
}
