use std::collections::HashSet;
use std::error::Error;
use std::fs::read_to_string;
use std::path::Path;
use std::time::Instant;

fn main() -> Result<(), Box<dyn Error>> {
    let then = Instant::now();
    println!("{}", parta("./data/09/input.txt")?);
    println!("{:?}", Instant::elapsed(&then));

    let then = Instant::now();
    println!("{}", partb("./data/09/input.txt")?);
    println!("{:?}", Instant::elapsed(&then));

    Ok(())
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
struct Pos {
    x: i32,
    y: i32,
}

struct Rope<const N: usize> {
    knot: [Pos; N],
    tail_pos: HashSet<Pos>,
}

impl<const N: usize> Rope<N> {
    fn new() -> Self {
        Self {
            knot: [Pos { x: 0, y: 0 }; N],
            tail_pos: HashSet::new(),
        }
    }

    fn move_rope(&mut self, direction: &str, mut count: i32) {
        while count > 0 {
            count -= 1;
            self.move_head(direction);
            self.move_tail();
        }
    }

    fn move_head(&mut self, direction: &str) {
        match direction {
            "U" => self.knot[0].y += 1,
            "D" => self.knot[0].y -= 1,
            "R" => self.knot[0].x += 1,
            "L" => self.knot[0].x -= 1,
            _ => unreachable!(),
        }
    }

    fn move_tail(&mut self) {
        for k in 1..N {
            let distx = self.knot[k - 1].x - self.knot[k].x;
            let disty = self.knot[k - 1].y - self.knot[k].y;
            if self.knot[k - 1].y == self.knot[k].y && distx.abs() >= 2 {
                self.knot[k].x += distx / distx.abs();
            } else if self.knot[k - 1].x == self.knot[k].x && disty.abs() >= 2 {
                self.knot[k].y += disty / disty.abs();
            } else if self.knot[k - 1].x != self.knot[k].x
                && self.knot[k - 1].y != self.knot[k].y
                && (distx.abs() >= 2 || disty.abs() >= 2)
            {
                self.knot[k].x += distx / distx.abs();
                self.knot[k].y += disty / disty.abs();
            }
        }
        self.tail_pos.insert(self.knot[N - 1]);
    }

    fn tail_pos_count(&self) -> usize {
        self.tail_pos.len()
    }
}

fn parta<P: AsRef<Path>>(filename: P) -> Result<usize, Box<dyn Error>> {
    let src = read_to_string(filename)?;
    let mut rope = Rope::<2>::new();

    for l in src.lines() {
        let Some((direction, countstr)) = l.split_once(' ') else {
            return Err("move format error".into());
        };
        let count = countstr.parse::<i32>()?;
        rope.move_rope(direction, count);
    }
    Ok(rope.tail_pos_count())
}

fn partb<P: AsRef<Path>>(filename: P) -> Result<usize, Box<dyn Error>> {
    let src = read_to_string(filename)?;
    let mut rope = Rope::<10>::new();

    for l in src.lines() {
        let Some((direction, countstr)) = l.split_once(' ') else {
            return Err("move format error".into());
        };
        let count = countstr.parse::<i32>()?;
        rope.move_rope(direction, count);
    }
    Ok(rope.tail_pos_count())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parta() {
        assert_eq!(parta("./data/09/sample.txt").unwrap(), 13);
    }

    #[test]
    fn test_partb_1() {
        assert_eq!(partb("./data/09/sample.txt").unwrap(), 1);
    }

    #[test]
    fn test_partb_2() {
        assert_eq!(partb("./data/09/sample2.txt").unwrap(), 36);
    }
}
